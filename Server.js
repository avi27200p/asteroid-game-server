class OnlineUser {
    constructor(socket, name){
      this.socket = socket,
      this.Name = name;
      this.FreeToPlay = true;
      this.Points = 0;
      this.IsUserLost = false;
    }
  }

const database = require("./Database")
const http = require('http');
const express = require('express')
const app = express()
const server = http.createServer(app);
const socketio = require('socket.io');
const { Console } = require("console");
const io = socketio(server);

let OnlineUsersList = [];

//#region Websocket communication

io.on('connection', (socket) =>{
    socket.on('ReadyToPlay', (name) => {
        if(OnlineUsersList.findIndex(u => u.Name == name) == -1)
            OnlineUsersList.push(new OnlineUser(socket, name));
        else
            OnlineUsersList.find(u => u.Name == name).FreeToPlay = true;
        UpdateUsersOnNewConnection();
    });
    socket.on("WantToPlay", (requestedGame) =>{
        HandlePlayRequest(JSON.parse(requestedGame));
    });
    socket.on("AgreeToPlay", (requestPlayData) => {
        HandleStartPlayRequest(JSON.parse(requestPlayData));
    });
    socket.on("RefuseToPlay", (refuseData) => {
        HandleRefusePlayRequest(JSON.parse(refuseData));
    });
    socket.on("UpdatePoints", (playData) => {
        HandleUpdatePoints(JSON.parse(playData));
    });
    socket.on("EndGame", (playData) => {
        HandleEndGame(JSON.parse(playData));
    });
});
io.on('disconnect', (sock) => {
    OnlineUsersList.splice(OnlineUsersList.findIndex(u => u.socket == socket), 1);
});
//#endregion 

//#region Get request
app.get("/GetLeaderboard", (req, res) => {
    database.GetLeaderboard((result) => {
        res.send(result);
    });
});

app.get("/Login", (req, res) => {
    database.CheckLogin(req.query, (user) => {
        res.send(user[0]);
    });
});

app.get("/IsEmailExist", (req,res) => {
    database.IsEmailAlreadyExist(req.query.Email, (result) => {
        res.send(result);
    });
});

app.get("/IsNameExist", (req,res) => {
    database.IsUsernameAlreadyExist(req.query, (result) => {
        res.send(result);
    });
});

app.get("/IsPhoneExist", (req,res) => {
    database.IsPhoneAlreadyExist(req.query, (result) => {
        res.send(result);
    });
});

app.get("/GetUserShipList", (req, res) => {
    database.GetUserShipList(req.query, (result) => {
        res.send(result);
    });
});

app.get("/CheckRivalPoints", (req,res) => {
    res.send(JSON.stringify(OnlineUsersList[OnlineUsersList.findIndex(user => user.Name == req.query.RivalName)].Points));
});

app.get("/IsUserWin", (req,res) => {
    res.send(JSON.stringify(OnlineUsersList.find(user => user.Name == req.query.RivalName).IsUserLost));
});
//#endregion

//#region  Post request
app.post("/ResetPassword", (req,res) => {
    database.ResetPassword(req.body);
});

app.post("/AddNewUser", (req, res) => {
    database.AddNewUser(req.body);
});

app.post("/AddShip", (req, res) => {
    database.AddShip(req.body);
});

app.post("UpdateCoinsAmount", (req, res) => {
    database.UpdateCoins(params);
});
//#endregion

//#region Handle websocket events methods

function HandlePlayRequest(requestedGame){
    OnlineUsersList.find(u => u.Name == requestedGame.RequestedPlayerName).socket.emit("WantedPlay", requestedGame.RequestingPlayerName)
}

async function HandleStartPlayRequest(playData){
    OnlineUsersList.find(u => u.Name == playData.Player1).socket.emit("StartPlay");
    OnlineUsersList.find(u => u.Name == playData.Player2).socket.emit("StartPlay");
    index1 = OnlineUsersList.findIndex(u => u.Name == playData.Player1);
    index2 = OnlineUsersList.findIndex(u => u.Name == playData.Player2); 
    ChangePlayersStatus(index1, index2);
}
  
function ChangePlayersStatus(index1, index2){
    OnlineUsersList[index1].FreeToPlay = false;
    OnlineUsersList[index2].FreeToPlay = false;
    OnlineUsersList[index1].Points = 0;
    OnlineUsersList[index2].Points = 0;
    OnlineUsersList[index1].IsUserLost = false;
    OnlineUsersList[index2].IsUserLost = false;
}

function HandleRefusePlayRequest(refuseData){
    OnlineUsersList.find(u => u.Name == refuseData.refusedName).socket.emit("RefusedPlay");
}

function HandleUpdatePoints(playData){
    OnlineUsersList.find(u => u.Name == playData.Username).Points = playData.Points;
}

function HandleEndGame(playData){
    OnlineUsersList.find(u => u.Name == playData.Player).IsUserLost = true;
}

function UpdateUsersOnNewConnection(){
    for(let index = 0; index < OnlineUsersList.length; index++){
        OnlineUsersList[index].socket.emit("OnlineFreeUsers", OnlineUsersList.filter(user => user.socket.id != OnlineUsersList[index].socket.id && user.FreeToPlay).map(user => user.Name))
    }
}
//#endregion
io.listen(3001);
app.listen(3000, () => {
    console.log(`Server started !`)
});