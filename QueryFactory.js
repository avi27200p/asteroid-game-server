function GetLeaderboardQuery(){
    return "SELECT users.Name, leaderboard.UserScore FROM asteroidsdb.leaderboard as leaderboard " +
           "INNER JOIN asteroidsdb.users as users ON users.id = leaderboard.UserId " +
           "ORDER BY leaderboard.UserScore DESC";
}

function GetLoginQuery(){
    return "SELECT * FROM users WHERE (Name = ? OR Email = ?) AND Password = ?";
}

function GetIsEmailExistQuery(){
    return "SELECT * FROM users WHERE Email = ?";
};

function GetIsUsernameExistQuery(){
    return "SELECT * FROM users WHERE Name = ?";
};

function GetIsPhoneExistQuery(){
    return "SELECT * FROM users WHERE PhoneNumber = ?";
};

function GetResetPasswordQuery(){
    return "INSERT INTO users SET Password = ? WHERE Email = ?";
}

function GetNewUserQuery(){
    return "INSERT INTO users values (?, ?, ?, ?)";
}

function GetStartingShipQuery(){
    return "INSERT INTO ships(Name) values (?)";
}

function GetAddShipQuery(){
    return "UPDATE ships SET ? = 1 WHERE UserId = (SELECT id FROM users WHERE Name = ?)"
}

function GetUpdateCoinsQuery(){
    return "UPDATE users SET Coins = ? WHERE Name = ?";
}

function GetUserShipListQuery(){
    return "SELECT ships.Definat, ships.BWing, ships.YWing FROM ships " + 
           "INNER JOIN users ON ships.UserId = users.id WHERE users.Name = ?";
}

module.exports = {
    GetLeaderboardQuery,
    GetLoginQuery,
    GetIsEmailExistQuery,
    GetIsUsernameExistQuery,
    GetResetPasswordQuery,
    GetNewUserQuery,
    GetStartingShipQuery,
    GetAddShipQuery,
    GetUpdateCoinsQuery,
    GetUserShipListQuery,
    GetIsPhoneExistQuery
};