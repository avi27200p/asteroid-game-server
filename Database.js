const QueryFactory = require("./QueryFactory");
var query;
let LastTestId = 0;
const mysql = require('mysql2');
    const connection = mysql.createConnection({
      host: "localhost",
      user: "root",
      password: "AvI243AvI",
      database: "asteroidsdb"
    });
    connection.connect(error => {
    if (error) {
        console.error(error);
    } else {
        console.log('Successfully connected to the database');
    }
    });

//#region Get Queries

function GetLeaderboard(callback){
    query = QueryFactory.GetLeaderboardQuery();
    connection.query(query, (err, result, fields) => {       
        callback(result);
    });
}

function CheckLogin(params, callback){
    query = QueryFactory.GetLoginQuery();
    connection.query(query, [params.Name, params.Email, params.Password], (err, result, fields) => {
        callback(result);
    });
}

function IsEmailAlreadyExist(email, callback){
    query = QueryFactory.GetIsEmailExistQuery();
    connection.query(query, [email], (err, result, fields) => {
        if(result.length)
            callback(true);
        else
            callback(false);
    });
}

function IsUsernameAlreadyExist(params, callback){
    query = QueryFactory.GetIsUsernameExistQuery();
    connection.query(query, [params.Name], (err, result, fields) => {
        if(result.length)
            callback(true);
        else
            callback(false);
    });
}

function IsPhoneAlreadyExist(params, callback){
    query = QueryFactory.GetIsPhoneExistQuery();
    connection.query(query, [params.PhoneNumber], (err, result, fields) => {
        if(result.length)
            callback(true);
        else
            callback(false);
    });
}

function GetUserShipList(params, callback){
    query = QueryFactory.GetUserShipListQuery();
    connection.query(query, [params.Name], (err, result, fields) => {
        callback(result[0]);
    });
}
//#endregion

//#region  Set Queries

function ResetPassword(params){
    query = QueryFactory.GetResetPasswordQuery();
    connection.query(query, [params.Password, params.Email]);
}

function AddNewUser(params){
    query = QueryFactory.GetNewUserQuery();
    connection.query(query, [params.Name, params.Email, params.Password, params.PhoneNumber])
    SetStartingShipForUser(params.Name);
}

function SetStartingShipForUser(name){
    query = QueryFactory.GetStartingShipQuery();
    connection.query(query, [name]);
}

function AddShip(params){
    query = QueryFactory.GetAddShipQuery();
    connection.query(query, [params.Ship, params.Name]);
}

function UpdateCoins(params){
    query = QueryFactory.GetUpdateCoinsQuery();
    connection.query(query, [params.Coins, params.Name]);
}
//#endregion

module.exports = {
    GetLeaderboard,
    CheckLogin,
    IsEmailAlreadyExist,
    IsUsernameAlreadyExist,
    IsPhoneAlreadyExist,
    ResetPassword,
    AddNewUser,
    AddShip,
    UpdateCoins,
    GetUserShipList
};